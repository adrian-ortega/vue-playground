export const resetFormFields = ({commit}) => {
  commit('UPDATE_FORM_FIELDS', {})
}

export const updateFormFields = ({commit, getters}, newFields) => {
  const oldFields = getters.form
  commit('UPDATE_FORM_FIELDS', {...oldFields, ...newFields})
}

export const setMaxSteps = ({commit}, max) => {
  commit('SET_MAX_STEPS', max)
}

export const nextStep = ({commit, getters}) => {
  const current = getters.step

  // .. whats our max?

  commit('UPDATE_STEP', current + 1)
}

export const previousStep = ({commit, getters}) => {
  const current = getters.step

  // .. what happens when we hit zero?

  commit('UPDATE_STEP', current - 1)
}
