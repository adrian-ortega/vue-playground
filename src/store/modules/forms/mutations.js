export const SET_MAX_STEPS = (state, max) => {
  state.maxSteps = max
}
export const UPDATE_STEP = (state, step) => {
  state.step = step
}

export const UPDATE_FORM_FIELDS = (state, form) => {
  state.form = form
}
