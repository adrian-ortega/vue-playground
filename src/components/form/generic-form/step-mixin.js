import {mapActions, mapGetters} from 'vuex'

export default {
  computed: {
    ...mapGetters({
      form: 'forms/form'
    })
  },
  methods: {
    ...mapActions({
      updateFormFields: 'forms/updateFormFields'
    }),
    update (field, value) {
      this.updateFormFields({
        [field]: value
      })
    }
  }
}
