export const step = (state) => state.step

export const minSteps = (state) => state.minSteps

export const maxSteps = (state) => state.maxSteps

export const form = (state) => state.form
